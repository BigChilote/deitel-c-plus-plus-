// Fig 3.12: GradeBook.cpp
// GradeBook memfer-function definitions. This file contains
// implementations of the member function prototyped in GradeBook.h
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook
using namespace std; 

// constructor initialized courseName with string supplied as argument
GradeBook::GradeBook( string name )
		: courseName( name ) // member initializer to initialize courseName
{
	// empty body
} // end GradeBook constructor

// function to get the course name
void GradeBook::setCourseName( string name )
{
	courseName = name; // store the course name in the object
} // end function setCourseName

// funtion to get the course name
string GradeBook::getCourseName() const
{
	return courseName; // return object's courseName
} // end function getCourseName

// display a welcome message to the GradeBook user
void GradeBook::displayMessage() const
{
	// call getCourseName to get the courseName
	std::cout << "Welcome to the grade book for\n" << getCourseName()
		<< "!" << std::endl;
} // end function displaMessage
