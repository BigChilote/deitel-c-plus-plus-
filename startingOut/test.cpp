// This program displays the values of true and false states.
#include <iostream>
using namespace std;

int main()
{
	bool a, b, c, d, e, f, g;
	int x = 5, y = 10, z = 8;

	a = x == 5 ;
	b = 7 <= (x + 2);
    c = z < 4;
    d = (2 + x) != y;
    e = z != 4;
    f = x >= 9;
    g = x <= (y * 2);

	cout << "a is  " << a << endl;
    cout << "b is  " << b << endl;
    cout << "c is  " << c << endl;
	cout << "d is  " << d << endl;
	cout << "e is  " << e << endl;
	cout << "f is  " << f << endl;
	cout << "g is  " << g << endl;

	return 0;
}
